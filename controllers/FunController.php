<?
class FunController {
	
	public function __construct() {
		$this->app = Application::getInstance();
	}
	
	public function jokeAction() {
		$agent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.872.0 Safari/535.2';
		$url = "http://shortiki.com/rss.php";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$result=curl_exec ($ch);
		curl_close ($ch);
		
		//$result = Net::get($url);
		$rss = new SimpleXMLElement($result);
		$c = count($rss->channel->item);
		$f = 0;
		if($c == 0) {
			$f = 1;
			$c = count($rss->description);
			if($c == 0) {
				$this->app->sayText("Ошибка чтения. Повторите команду.");;
			}
		}
		$r = rand(0, $c-1);
		if($f)
			$text = strip_tags($rss->description[$r]);
		else
			$text = strip_tags($rss->channel->item[$r]->description);
		$text = preg_replace("/[^A-Za-zА-Яа-я0-9\.\!\?\s]/u", "", $rss->channel->item[$r]->description);
		print($text);
		$this->app->sayText($text);
	}
	
	public function radioAction($cmd) {
		if(preg_match("/выключ/", $cmd)) {
			exec("killall mplayer");
			return;
		}
		$command = "mplayer ";
		$stationsList = $this->app->getDict("radio");
		foreach($stationsList as $sName=>$sData) {
			if(preg_match($sName, $cmd)) {
				$this->app->sayText("Радио " . $sData[1]);
				$command .= $sData[0];
				exec($command . " > /dev/null &");
				return;
			}
		}
		$this->app->sayText("Я не знаю такой станции!");
		return;
	}
	
}
