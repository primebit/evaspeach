<?
class SystemController {
	
	public function __construct() {
		$this->app = Application::getInstance();
	}
	
	public function iamhereAction() {
		$phrases = $this->app->getDict("anythings");
		$this->app->sayText($phrases[rand(0,count($phrases))]);
	}
	
	public function quitAction() {
		$this->app->sayText("I'll be back");
		exit(0);
	}
	
}
