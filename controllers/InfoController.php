<?
class InfoController {
	
	public function __construct() {
		$this->app = Application::getInstance();
	}
	
	public function clockAction($cl) {
		$now = date('Y-m-d-G-i');
		list($y, $m, $d, $h, $min) = split("-", $now);
		
		if($h == 0) $h = "ноль";
		if($h != 10 && strlen($h) == 2) $f_h = substr($h,1,1); else $f_h = $h;
		if($f_h == 1) $f_h = $h . " час";
		if($f_h == 2 || $f_h == 3 || $f_h == 4) $f_h = $h . " часа";
		if(in_array($f_h, array(0,5,6,7,8,9,10))) $f_h = $h . " часов";
		
		if($min == 0) $min = "ноль";
		if($min != 10 && strlen($min) == 2) $f_min = substr($min,1,1); else $f_min = $min;
		if($f_min == 1) $f_min = $min . " минута";
		if($f_min == 2 || $f_min == 3 || $f_min == 4) $f_min = $min . " минуты";
		if(in_array($f_min, array(0,5,6,7,8,9,10))) $f_min = $min . " минут";
		
		$this->app->sayText("Точное время - " . $f_h . $f_min);
	}
	
	public function weatherAction() {
		include_once("lib/YahooWeather.php");
		try {
			$weather = new YahooWeather('RSXX0091');
			
			$wd = "";
			if($weather->wind_direction > 30 ||  $weather->wind_direction < 60)
				$wd = "северо-восточный";
			elseif($weather->wind_direction >= 60 ||  $weather->wind_direction <= 120)
				$wd = "восточный";
			elseif($weather->wind_direction > 120 &&  $weather->wind_direction < 150)
				$wd = "юго-восточный";
			elseif($weather->wind_direction >= 150 ||  $weather->wind_direction <= 210)
				$wd = "южный";
			elseif($weather->wind_direction > 210 &&  $weather->wind_direction < 240)
				$wd = "юго-западный";
			elseif($weather->wind_direction >= 240 ||  $weather->wind_direction <= 300)
				$wd = "западный";
			elseif($weather->wind_direction > 300 &&  $weather->wind_direction < 330)
				$wd = "северо-западный";
			else
				$wd = "северный";
				
			$t = "";
			$t = ($weather->temp > 0) ? "выше нуля" : "ниже нуля";
			
			$text = "Температура воздуха - " . abs($weather->temp) . " градусов Цельсия " . $t . ". ";
			$text .= "Ветер " . $wd . ", " . $weather->wind_speed . " метров в секунду.";
			$this->app->sayText($text);
		} catch(Exception $e) {
			echo "Caught exception: ".$e->getMessage();
			exit();
		}
	}
	
}
