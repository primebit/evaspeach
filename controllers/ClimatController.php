<?
class ClimatController {
	
	protected $app = NULL;
	
	public function __construct() {
		$this->app = Application::getInstance();
	}
	
	public function summaryAction() {
		$memcache = $this->app->getMC();
		//$asd = Application::getInstance()->getDict("anythings");
		$data = $memcache->get("climat_temp");
		$this->app->sayText("Температура в комнате - " . substr($data,0,2) . " градусов Цельсия");
	}
}
