<?
return array(
	"/врем/" => array("info", "clock"),
	"/погод/" => array("info", "weather"),
	
	"/умри/" => array("system", "quit"),
	"/(проверк).*(связ)/" => array("system", "iamhere"),
	
	"/прикол/" => array("fun", "joke"),
	"/пошути/" => array("fun", "joke"),
	"/радио/" => array("fun", "radio"),
	
	"/климат/" => array("climat", "summary"),
);
