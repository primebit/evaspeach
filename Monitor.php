<?
include("lib/Serial.php");

$memcache_obj = new Memcache;
$memcache_obj->connect('127.0.0.1', 11211) or die("Could not connect to memcached");

$serial = new phpSerial();
$serial->deviceSet("/dev/ttyACM0");
$serial->confBaudRate(9600);  
$serial->deviceOpen();

$read = "";
while(true) {
	//$old = $memcache_obj->get('climat_temp');
	//if(!empty($old)
	//	echo "OLD: " .$old. "\n";
	$read = $serial->readPort();
	$t = array();
	preg_match("/\d{4}/",$read,$t);
	if(!empty($t[0])) {
		$read = $t[0];
		echo $read."\n";
		$memcache_obj->delete('climat_temp');
		$memcache_obj->set('climat_temp', $read, false, 5);
		$read = "";
		sleep(1);
	}
}

$serial->deviceClose(); 
