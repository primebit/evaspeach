<?
include("lib/Net.php");

class Application {
	
	static protected $_instance = NULL;
	
	protected
		$config = array(),
		$signs = array(),
		$dicts = array(),
		$controllers = array(),
		$memcache = NULL;
	
	private function __construct() {}
	
	static public function getInstance() {
		if(self::$_instance == NULL)
			self::$_instance = new self();
		return self::$_instance;
	}
	
	public function init() {
		$this->loadResources();
	}
	
	protected function loadResources() {
		$this->config = include("data/config.php");
		$this->signs = include("data/signs.php");
		$this->dicts = include("data/dicts.php");
	}
	
	public function checkCmd($cmd) {
		foreach($this->signs as $sign=>$ca) {
			if(preg_match($sign, $cmd)) {
				$controller = $this->getController($ca[0]);
				$action = $ca[1] . "Action";
				return $controller->$action($cmd);
			}
			$cmd .= " ";
		}
		if(trim($cmd) == "ева")
			$this->sayText("Слушаю!");
		else
			$this->sayText("Не поняла.");
		return false;
	}
	
	public function sayText($text) {
		$fp = fsockopen("127.0.0.1", 16000, $errno, $errstr, 30);
		if (!$fp) {
			print("Ошибка подключения к голосовому серверу \n");
		} else {
			fwrite($fp, $text);
			fclose($fp);
		}
	}
	
	protected function getController($name) {
		if(!isset($this->controllers[$name])) {
			$cName = ucfirst($name)."Controller";
			include_once("controllers/" . $cName . ".php");
			$this->controllers[$name] = new $cName;
		}
		return $this->controllers[$name];
	}
	
	// Accessors
	
	public function getConfig($key) {
		if(!isset($this->config[$key]))
			$this->config[$key] = false;
		return $this->config[$key];
	}
	
	public function getDict($key) {
		if(!isset($this->dicts[$key]))
			$this->dicts[$key] = array();
		return $this->dicts[$key];
	}
	
	public function getMC() {
		if($this->memcache == NULL) {
			$this->memcache = new Memcache;
			$this->memcache->connect('127.0.0.1', 11211) or die("Could not connect to memcached");
		}
		return $this->memcache;
	}
	
}
