<?
class Net {
	
	static public function get($url) {
		$app = Application::getInstance();
		$aContext = array(
			'http' => array(
				'request_fulluri' => True,
			),
		);
		if($app->getConfig("proxy_use")) {
			$aContext['http']['proxy'] = 'tcp://192.168.0.254:3128';
		}
		$cxContext = stream_context_create($aContext);
		return file_get_contents($url, False, $cxContext);
	}
	
}
