<?
set_time_limit(0);
include("lib/Application.php");

Application::getInstance()->init();

while(true) {
	print("Запись... \n");
	exec("rec -q -c 1 -r 16000 ./tmp/current.wav silence 1 0.3 2% 1 0.3 2%");
	if(!file_exists('tmp/current.wav')) {
		print("Ничего не записано! \n");
	} else {
		print("Распознавание... \n");
		exec("sox ./tmp/current.wav ./tmp/current.flac");
		
		$file_to_upload = array('myfile'=>'@tmp/current.flac');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://www.google.com/speech-api/v1/recognize?xjerr=1&client=chromium&lang=ru-RU");
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: audio/x-flac; rate=16000"));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $file_to_upload);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$result=curl_exec ($ch);
		curl_close ($ch);
		
		$json_array = json_decode($result, true);
		
		if($json_array["status"] != 0) {
			print("Говори четче, хозяин! \n");
		} else {
			$t = NULL;
			$cmd = $json_array["hypotheses"][0]["utterance"];
			$cmd = strtolower($cmd);
			print("Ваша команда - " . $cmd . " (точность " . $json_array["hypotheses"][0]["confidence"] . ") \n");
			if(preg_match("/^ева/", $cmd)) {
				Application::getInstance()->checkCmd($cmd);
			} else {
				print("Это не ко мне \n");
			}
		}
		
	}
}
