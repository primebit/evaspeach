<?
set_time_limit(0);

$address = '127.0.0.1';
$port = 16000;
$max_clients = 10;
$client = Array(); 

$sock = socket_create(AF_INET, SOCK_STREAM, 0);
socket_bind($sock, $address, $port) or die('Could not bind to address');
socket_listen($sock); 

while(true) {
	$read[0] = $sock;
	for($i=0 ; $i<$max_clients ; $i++) {
		if($client[$i]['sock']  != null)
			$read[$i+1] = $client[$i]['sock']; 
	}
	$ready = socket_select($read, $writearr, $exceptarr, 0);
	if(in_array($sock, $read)) {
		for($i = 0 ; $i<$max_clients ; $i++) {
			if ($client[$i]['sock'] == null) {
				$client[$i]['sock'] = socket_accept($sock);
				break;
			} elseif ($i == $max_clients - 1) {
				print("too many clients");
			}
		}
		if(--$ready <= 0)
			continue;
	}
	
	for($i=0 ; $i<$max_clients ; $i++) {
		if(in_array($client[$i]['sock'] , $read)) {
			$input = socket_read($client[$i]['sock'] , 1000000);
			if ($input == null) {
				unset($client[$i]); 
			}
			sayText($input);
		}
	}
}

function sayText($text) {
	$agent = 'Mozilla/5.0 (X11; Linux i686; rv:7.0.1) Gecko/20100101 Firefox/7.0.1';
	$url = "http://translate.google.com/translate_tts?tl=ru&q=" . urlencode($text);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, $agent);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	$result=curl_exec ($ch);
	curl_close ($ch);
	file_put_contents("tmp/say.mp3", $result);
	//echo $result; exit();
	exec("mpg123 ./tmp/say.mp3");
}
